with Ada.Text_IO; use Ada.Text_IO;
with GNAT.Regpat; use GNAT.Regpat;
with Ada.Integer_Text_IO;
with Ada.Calendar; use Ada.Calendar;
use type Ada.Calendar.Time;
with Ada.Calendar.Formatting; use Ada.Calendar.Formatting;
with Ada.Calendar.Time_Zones; use Ada.Calendar.Time_Zones;

procedure Main is

   type Month_Name is
     (Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec);

   package Month_Name_IO is new Enumeration_IO (Month_Name);

   function To_Number (Name : Month_Name) return Month_Number is
   begin
      return Month_Number (Month_Name'Pos (Name) + 1);
   end To_Number;

   function To_Time (Input : String) return Ada.Calendar.Time is
      Day              : Day_Number;
      Month            : Month_Name;
      Year             : Year_Number;
      Hour             : Hour_Number;
      Minute           : Minute_Number;
      Second           : Second_Number;
      Time_Zone_Hour   : Natural;
      Time_Zone_Minute : Natural;
      I                : Positive;
      Zone_Sign        : Integer := 1;
   begin
      Ada.Integer_Text_IO.Get (From => Input, Item => Day, Last => I);
      Month_Name_IO.Get
        (From => Input (I + 2 .. Input'Last), Item => Month, Last => I);
      Ada.Integer_Text_IO.Get
        (From => Input (I + 2 .. I + 5), Item => Year, Last => I);
      Ada.Integer_Text_IO.Get
        (From => Input (I + 2 .. I + 3), Item => Hour, Last => I);
      Ada.Integer_Text_IO.Get
        (From => Input (I + 2 .. I + 3), Item => Minute, Last => I);
      Ada.Integer_Text_IO.Get
        (From => Input (I + 2 .. Input'Last), Item => Second, Last => I);
      if Input (I + 2) = '-' then
         Zone_Sign := -1;
      end if;
      Ada.Integer_Text_IO.Get
        (From => Input (I + 3 .. I + 4), Item => Time_Zone_Hour, Last => I);
      Ada.Integer_Text_IO.Get
        (From => Input (I + 1 .. I + 2), Item => Time_Zone_Minute, Last => I);

      return Time_Of
          (Year => Year
	  , Month => To_Number (Month)
	  , Day => Day
	  , Hour => Hour
	  , Minute => Minute
	  , Second => Second
	  , Time_Zone => Time_Offset (Zone_Sign * (Time_Zone_Hour * 60 + Time_Zone_Minute))
	  );
   end To_Time;

   Request_Matcher  : constant Pattern_Matcher :=
     Compile ("^(\S+) \S+ \S+ \[(.+)\] ""GET /escappes/(\d+)/episodes/(\d+) HTTP/1.1"" 200 \S+ "".*"" ""(.*)""");

   procedure Read_Line (Line : String) is
      Time             : Ada.Calendar.Time;
      Matches          : Match_Array (0 .. 5);
   begin
      Match (Request_Matcher, Line, Matches);
      if Matches (0) = No_Match then
         return;
      end if;
      Time := To_Time (Line (Matches(2).First .. Matches(2).Last));
      Put_Line
        ("select gimcana.record_visit" &
        "( ip_address => '" & Line (Matches (1).First .. Matches (1).Last) & "'::inet" &
        ", user_agent => '" & Line (Matches (5).First .. Matches(5).Last) & "'" &
        ", gymkhana => " & Line (Matches (3).First .. Matches(3).Last) &
        ", stage => " & Line (Matches (4).First .. Matches(4).Last) &
        ", date_time => '" & Ada.Calendar.Formatting.Image (Time) & "'::timestamp" &
        ");");
   end Read_Line;

begin
   while not End_Of_Line loop
      Read_Line (Get_Line);
   end loop;
end Main;
